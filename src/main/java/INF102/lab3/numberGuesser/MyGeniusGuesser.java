package INF102.lab3.numberGuesser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyGeniusGuesser implements IGuesser {

    @Override
    public int findNumber(RandomNumber number) {
        int lowerbound = number.getLowerbound();
        int upperbound = number.getUpperbound();
        List<Integer> possibleNumbers = new ArrayList<>();
        for (int i = lowerbound; i <= upperbound; i++) {
            possibleNumbers.add(i);
        }

        Collections.sort(possibleNumbers);
        int left = 0;
        int right = possibleNumbers.size() - 1;
        int guess = -1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int result = number.guess(possibleNumbers.get(mid));
            if (result == 0) {
                guess = possibleNumbers.get(mid);
                break;
            } else if (result < 0) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return guess;

    }
}
