package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if(numbers.isEmpty())
            throw new IllegalArgumentException("Empty list");
        // edge cases
        if (numbers.size() == 1)
            return numbers.get(0);
        if (numbers.get(0) > numbers.get(1))
            return numbers.get(0);
        if (numbers.get(numbers.size()-1) > numbers.get(numbers.size()-2))
            return numbers.get(numbers.size()-1);
        // recursive case
        return peakElement(numbers.subList(1, numbers.size()-1));
        
    }

}
