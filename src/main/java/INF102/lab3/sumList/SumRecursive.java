package INF102.lab3.sumList;

import java.util.ArrayList;
import java.util.List;

public class SumRecursive implements ISum {

  @Override
    public long sum(List<Long> list) {
        return sumHelper(list, 0);
    }

    private long sumHelper(List<Long> list, long accumulator) {
        if (list.isEmpty()) {
            return accumulator;
        } else {
            long value = list.get(0);
            List<Long> newList = list.subList(1, list.size());
            return sumHelper(newList, accumulator + value);
        }
    }
}

